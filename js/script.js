"use strict"

/*

1. Метод обєкту - це функція яка повязана з певним обєктом. Методи обєкта дозволяють виконувати додаткові дії з обєктом, розширюючи його фунціональність.

2. Властивості обєкта можуть мати будь які типи даних.
    Наприклад:
    Рядок String,
    Число Number,
    Масив Array,
    Обєкт Object,
    Булевий Boolean,
    Функція Function.

3. У JS обєкти є посилальним типом даних, це означає, що при створенні нового обєкту і присвоєння його змінній, ця змінна містить не сам обєкт а посилання або адресу памяті де зберігається цей обєкт.
    При створенні іншої змінної з посиланням на обєкт першої, вони посилаюстья на однаковий обєкт в памяті. І якщо змінити властивості в обєкта в іншій, вони змінться і в першій

*/



// ex 1

let product = {
    name: 'RTX 4090',
    price: 1300,
    discount: 15,
    calculatePrice: function () {
        let priceWithDiscount = this.price * (1 - this.discount / 100);
        return priceWithDiscount;
    }
};

let finalPrice = product.calculatePrice();

// console.log(`Full price with discount: ${finalPrice.toFixed(2)}$`);



// ex 2

function user() {
    let userName;
    let userAge;

    while (!userName || !userAge) {
        userName = prompt('Enter your name:');
        userAge = prompt('Enter your age:');

        if (!userName || !userAge) {
            alert('Please enter your name and age.');
        }
    }

    let hello = `Hello! My name is ${userName}, I'm ${userAge} years old.`;
    alert(hello);
}

// user();



// ex 3

function deepClone(obj, clonedObjects = new WeakMap()) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    if (clonedObjects.has(obj)) {
        return clonedObjects.get(obj);
    }

    if (Array.isArray(obj)) {
        const clonedArray = obj.map(item => deepClone(item, clonedObjects));
        clonedObjects.set(obj, clonedArray);
        return clonedArray;
    }

    if (typeof obj === 'string' || typeof obj === 'number' || typeof obj === 'boolean') {
        return obj;
    }

    const clonedObj = Object.create(null);
    clonedObjects.set(obj, clonedObj);
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            clonedObj[key] = deepClone(obj[key], clonedObjects);
        }
    }

    return clonedObj;

}

let userInfo = {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
        }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
    }
}


const clonedObj = deepClone(userInfo);
// console.log(clonedObj);
